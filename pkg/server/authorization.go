package server

import (
	"fmt"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"log"
	"net/http"
	"slices"
)

// authorization is the authorization endpoint
// It handles the authorization request from the client
// and redirects to the login page.
func (s *server) authorization(w http.ResponseWriter, r *http.Request) {
	input, err := NewAuthorizationInput(r)
	if err != nil {
		http.Error(w, "invalid request", http.StatusBadRequest)
		return
	}

	err = s.Validate.Struct(input)
	if err != nil {
		http.Error(w, fmt.Sprintf("Input not valid: %srv", err), http.StatusBadRequest)
		return
	}

	app := s.Config.FindAppWithClientId(input.ClientID)
	if app == nil {
		http.Error(w, fmt.Sprintf("invalid client %s", input.ClientID), http.StatusBadRequest)
		return
	}

	if !slices.Contains(app.RedirectURIs, input.RedirectURI) {
		http.Error(w, "invalid redirect_uri", http.StatusBadRequest)
		return
	}

	log.Println("Authorization request for client", input.ClientID)

	// register a session with the login request in the server
	sessionId, err := oidc.GetRandomString(64)
	if err != nil {
		http.Error(w, "internal error creating session id", http.StatusInternalServerError)
		return
	}
	s.LoginRequests[sessionId] = NewLoginRequest(input, *app)

	w.Header().Add(
		"Location",
		fmt.Sprintf("%s/login?session_id=%s", s.Config.Url, sessionId),
	)
	w.WriteHeader(http.StatusFound)

}
