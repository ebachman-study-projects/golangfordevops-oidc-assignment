package server

import "gopkg.in/yaml.v3"

func ReadConfig(bytes []byte) Config {
	var config Config
	config.LoadError = yaml.Unmarshal(bytes, &config)
	return config
}
