package server

import (
	"embed"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/users"
	"io"
	"net/http"
	"net/url"
)

//go:embed templates/*
var templateFs embed.FS

func (s *server) login(w http.ResponseWriter, r *http.Request) {
	// to access the login template:
	// templateFile, err := templateFs.Open("templates/login.html")
	switch r.Method {
	case http.MethodGet:
		loginGet(s, w, r)
	case http.MethodPost:
		loginPost(s, w, r)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func loginGet(s *server, w http.ResponseWriter, r *http.Request) {
	parsedUri, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	sessionId := parsedUri.Query().Get("session_id")
	_, ok := s.LoginRequests[sessionId]
	if !ok {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	template, err := readLoginTemplate()
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	_, err = w.Write([]byte(template))
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html")
}

func loginPost(s *server, w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	sessionId := r.Form.Get("session_id")
	if sessionId == "" {
		http.Error(w, "Bad request - no session", http.StatusBadRequest)
		return
	}

	loginRequest, ok := s.LoginRequests[sessionId]
	if !ok {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	login := r.FormValue("login")
	password := r.FormValue("password")
	if login == "" || password == "" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	found, user, err := users.Auth(login, password, "")
	if err != nil || !found {
		http.Error(w, "Unauthorized - user not found", http.StatusUnauthorized)
		return
	}

	// We send back a code to the redirect uri from the session
	authCode, err := oidc.GetRandomString(64)
	if err != nil {
		http.Error(w, "Internal server error creating random string", http.StatusInternalServerError)
		return
	}

	parsedRedirectUri, err := url.ParseRequestURI(loginRequest.RedirectURI)
	if err != nil {
		http.Error(w, "Internal server error - redirecturi", http.StatusInternalServerError)
		return
	}
	values := parsedRedirectUri.Query()
	values.Add("code", authCode)
	values.Add("state", loginRequest.State)
	parsedRedirectUri.RawQuery = values.Encode()
	redirectUri := parsedRedirectUri.String()

	// register data for the code, and remove the login request from the session
	s.Codes[authCode] = NewCodeRequest(loginRequest.App, user, loginRequest)
	delete(s.LoginRequests, sessionId)

	w.Header().Set("Location", redirectUri)
	w.WriteHeader(http.StatusFound)
}

func readLoginTemplate() (string, error) {
	templateFile, err := templateFs.Open("templates/login.html")
	if err != nil {
		return "", err
	}
	defer templateFile.Close()
	reader := io.Reader(templateFile)
	contents, err := io.ReadAll(reader)
	if err != nil {
		return "", err
	}
	return string(contents), nil
}
