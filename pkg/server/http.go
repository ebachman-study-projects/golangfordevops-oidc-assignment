package server

import (
	"github.com/go-playground/validator/v10"
	"net/http"
)

// server is the main struct for the server
type server struct {
	PrivateKey []byte
	Config     Config
	// LoginRequests is a map of login requests, where the key is the session id
	// In a production application, this would be a database like Redis
	LoginRequests map[string]LoginRequest
	// Codes is a map of code requests, where the key is the code
	// The code is used to exchange for an access token
	Codes map[string]CodeData
	// DI
	Validate *validator.Validate
}

func newServer(privateKey []byte, config Config) *server {
	return &server{
		PrivateKey:    privateKey,
		Config:        config,
		Validate:      validator.New(),
		LoginRequests: make(map[string]LoginRequest),
		Codes:         make(map[string]CodeData),
	}
}

func Start(httpServer *http.Server, privateKey []byte, config Config) error {
	s := newServer(privateKey, config)

	http.HandleFunc("/authorization", s.authorization)
	http.HandleFunc("/token", s.token)
	http.HandleFunc("/login", s.login)
	http.HandleFunc("/jwks.json", s.jwks)
	http.HandleFunc("/.well-known/openid-configuration", s.discovery)
	http.HandleFunc("/userinfo", s.userinfo)

	return httpServer.ListenAndServe()
}
