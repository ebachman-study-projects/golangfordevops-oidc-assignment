package server

import (
	"encoding/json"
	"github.com/golang-jwt/jwt/v4"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"net/http"
	"net/url"
	"time"
)

// RequestTokenFormData is the input for the token endpoint,
// it is parsed from the request
type RequestTokenFormData struct {
	GrantType    string `validate:"required,eq=authorization_code"`
	Code         string `validate:"required"`
	ClientID     string `validate:"required"`
	ClientSecret string `validate:"required"`
	RedirectURI  string `validate:"required"`
}

// NewRequestTokenFormData creates a new RequestTokenFormData from the given url.Values
func NewRequestTokenFormData(v url.Values) (RequestTokenFormData, error) {
	return RequestTokenFormData{
		GrantType:    v.Get("grant_type"),
		Code:         v.Get("code"),
		ClientID:     v.Get("client_id"),
		ClientSecret: v.Get("client_secret"),
		RedirectURI:  v.Get("redirect_uri"),
	}, nil
}

func (s *server) token(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if err := r.ParseForm(); err != nil {
		http.Error(w, "invalid request", http.StatusBadRequest)
		return
	}
	tokenInput, err := NewRequestTokenFormData(r.PostForm)
	if err != nil {
		http.Error(w, "invalid request", http.StatusBadRequest)
		return
	}
	err = s.Validate.Struct(tokenInput)
	if err != nil {
		http.Error(w, "invalid request", http.StatusBadRequest)
		return
	}

	if tokenInput.GrantType != "authorization_code" {
		http.Error(w, "grant type not supported", http.StatusBadRequest)
		return
	}

	loginRequest, ok := s.Codes[tokenInput.Code]
	if !ok {
		http.Error(w, "code is invalid", http.StatusBadRequest)
		return
	}
	if time.Now().After(loginRequest.CreatedAt.Add(10 * time.Minute)) {
		http.Error(w, "code is expired", http.StatusBadRequest)
		return
	}
	if tokenInput.ClientID != loginRequest.App.ClientID {
		http.Error(w, "client id does not match", http.StatusBadRequest)
		return
	}
	if tokenInput.ClientSecret != loginRequest.App.ClientSecret {
		http.Error(w, "client secret does not match", http.StatusBadRequest)
		return
	}
	if tokenInput.RedirectURI != loginRequest.LoginRequest.RedirectURI {
		http.Error(w, "redirect uri does not match", http.StatusBadRequest)
		return
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(s.PrivateKey)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	// Id token validates whether we authenticated the user
	idTokenClaims := jwt.MapClaims{
		"sub": loginRequest.User.Sub,
		"iss": s.Config.Url,
		"aud": loginRequest.App.ClientID,
		"exp": time.Now().Add(1 * time.Hour).Unix(),
		"nb":  time.Now().Unix(),
		"iat": time.Now().Unix(),
	}
	idToken := jwt.NewWithClaims(jwt.SigningMethodRS256, idTokenClaims)
	idToken.Header["kid"] = "0-0-0-1"
	signedIdToken, err := idToken.SignedString(privateKey)

	// Access token is used to access resources
	// The audiences are the resources that can be accessed
	accessTokenClaims := jwt.MapClaims{
		"sub": loginRequest.User.Sub,
		"iss": s.Config.Url,
		"aud": []string{
			s.Config.Url + "/userinfo",
		},
		"exp": time.Now().Add(1 * time.Hour).Unix(),
		"nb":  time.Now().Unix(),
		"iat": time.Now().Unix(),
	}
	accessToken := jwt.NewWithClaims(jwt.SigningMethodRS256, accessTokenClaims)
	accessToken.Header["kid"] = "0-0-0-1"
	signedAccessToken, err := accessToken.SignedString(privateKey)

	tokenResponse := oidc.Token{
		AccessToken: signedAccessToken,
		IDToken:     signedIdToken,
		TokenType:   "Bearer",
	}
	responseBody, err := json.Marshal(tokenResponse)
	if err != nil {
		http.Error(w, "could not marshal response", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(responseBody)
	if err != nil {
		http.Error(w, "could not write response", http.StatusInternalServerError)
		return
	}

}
