package server

import (
	"fmt"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/users"
	"net/http"
	"net/url"
	"time"
)

// Config is the configuration for the server
type Config struct {
	Apps      map[string]AppConfig `yaml:"apps"`
	Url       string               `yaml:"url"`
	LoadError error
}

func (c Config) Validate() error {
	if c.LoadError != nil {
		return c.LoadError
	}
	if c.Url == "" {
		return fmt.Errorf("url is required")
	}
	for name, app := range c.Apps {
		if err := app.Validate(); err != nil {
			return fmt.Errorf("app %s: %w", name, err)
		}
	}
	return nil
}

// findAppWithClientId returns the app with the given client id or nil if not found
func (c Config) FindAppWithClientId(clientId string) *AppConfig {
	for _, app := range c.Apps {
		if app.ClientID == clientId {
			return &app
		}
	}
	return nil
}

// AppConfig is the configuration for an app
type AppConfig struct {
	ClientID     string   `yaml:"client_id"`
	ClientSecret string   `yaml:"client_secret"`
	Issuer       string   `yaml:"issuer"`
	RedirectURIs []string `yaml:"redirect_uris"`
}

func (c AppConfig) Validate() error {
	if c.ClientID == "" {
		return fmt.Errorf("client_id is required")
	}
	if c.ClientSecret == "" {
		return fmt.Errorf("client_secret is required")
	}
	if c.Issuer == "" {
		return fmt.Errorf("issuer is required")
	}
	if len(c.RedirectURIs) == 0 {
		return fmt.Errorf("redirect_uris is required")
	}
	return nil
}

// AuthorizationInput is the input for the authorization endpoint,
// it is parsed from the request
type AuthorizationInput struct {
	ClientID     string `validate:"required"`
	RedirectURI  string `validate:"required"`
	ResponseType string `validate:"eq=code"`
	Scope        string `validate:"eq=openid"`
	State        string `validate:"required"`
}

func NewAuthorizationInput(r *http.Request) (AuthorizationInput, error) {
	parsedUri, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		return AuthorizationInput{}, err
	}
	query := parsedUri.Query()

	return AuthorizationInput{
		ClientID:     query.Get("client_id"),
		RedirectURI:  query.Get("redirect_uri"),
		ResponseType: query.Get("response_type"),
		Scope:        query.Get("scope"),
		State:        query.Get("state"),
	}, nil
}

// LoginRequest is the request to the login endpoint and
// stored in a session
type LoginRequest struct {
	ClientID     string
	RedirectURI  string
	Scope        string
	ResponseType string
	State        string
	App          AppConfig
	CreatedAt    time.Time
}

func NewLoginRequest(input AuthorizationInput, app AppConfig) LoginRequest {
	return LoginRequest{
		ClientID:     input.ClientID,
		RedirectURI:  input.RedirectURI,
		Scope:        input.Scope,
		ResponseType: input.ResponseType,
		State:        input.State,
		App:          app,
		CreatedAt:    time.Now(),
	}
}

// CodeData is the data for a code
type CodeData struct {
	App          AppConfig
	User         users.User
	CreatedAt    time.Time
	LoginRequest LoginRequest
}

func NewCodeRequest(app AppConfig, user users.User, loginRequest LoginRequest) CodeData {
	return CodeData{
		App:          app,
		User:         user,
		LoginRequest: loginRequest,
		CreatedAt:    time.Now(),
	}
}
