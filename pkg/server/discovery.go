package server

import (
	"bytes"
	"encoding/json"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"net/http"
)

func (s *server) discovery(w http.ResponseWriter, r *http.Request) {
	e := oidc.Discovery{
		Issuer:                            s.Config.Url,
		AuthorizationEndpoint:             s.Config.Url + "/authorization",
		TokenEndpoint:                     s.Config.Url + "/token",
		JwksURI:                           s.Config.Url + "/jwks.json",
		UserinfoEndpoint:                  s.Config.Url + "/userinfo",
		ScopesSupported:                   []string{"openid"},
		ResponseTypesSupported:            []string{"code"},
		TokenEndpointAuthMethodsSupported: []string{"none"},
	}
	payload, err := json.Marshal(e)
	if err != nil {
		http.Error(w, "Internal server error (json marshalling)", http.StatusInternalServerError)
		return
	}

	formattedPayload := bytes.NewBuffer([]byte{})
	err = json.Indent(formattedPayload, payload, "", "  ")
	if err != nil {
		http.Error(w, "Internal server error (json formatting)", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(formattedPayload.Bytes())
	if err != nil {
		http.Error(w, "Internal server error (writing response)", http.StatusInternalServerError)
		return
	}
}
