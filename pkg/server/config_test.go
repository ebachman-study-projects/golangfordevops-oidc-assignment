package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var yamldata = `
url: "https://www.example.com"
apps:
  app1:
    client_id: "1-2-3-4"
    client_secret: "secret"
    issuer: "http://localhost:8080"
    redirect_uris:
      - "http://localhost:8082/callback"
      - "http://localhost:8083/callback"
  app2:
`

func TestReadConfig(t *testing.T) {
	config := ReadConfig([]byte(yamldata))
	if config.LoadError != nil {
		t.Errorf("Error loading config: %s", config.LoadError)
	}
	assert.Equal(t, "https://www.example.com", config.Url)
	assert.Len(t, config.Apps, 2)
	_, ok := config.Apps["app1"]
	assert.True(t, ok)
	_, ok = config.Apps["app2"]
	assert.True(t, ok)
	_, ok = config.Apps["app3"]
	assert.False(t, ok)

	assert.Equal(t, "1-2-3-4", config.Apps["app1"].ClientID)
	assert.Equal(t, "secret", config.Apps["app1"].ClientSecret)
	assert.Equal(t, "http://localhost:8080", config.Apps["app1"].Issuer)
	assert.Len(t, config.Apps["app1"].RedirectURIs, 2)
	assert.Equal(t, "http://localhost:8082/callback", config.Apps["app1"].RedirectURIs[0])
	assert.Equal(t, "http://localhost:8083/callback", config.Apps["app1"].RedirectURIs[1])

	assert.NotNil(t, config.Validate(), "config should be valid")
	assert.Nilf(t, config.Apps["app1"].Validate(), "app1 should be valid")
	assert.NotNil(t, config.Apps["app2"].Validate(), "app2 should be invalid")
}
