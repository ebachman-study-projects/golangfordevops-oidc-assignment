package server

import (
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/users"
	"math/big"
	"net/http"
	"slices"
	"strings"
)

func (s *server) userinfo(w http.ResponseWriter, r *http.Request) {
	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken[:7] != "Bearer " {
		http.Error(w, "Not a bearer token", http.StatusUnauthorized)
		return
	}
	rawAccessToken = strings.TrimPrefix(rawAccessToken, "Bearer ")

	claims := &jwt.RegisteredClaims{}
	_, err := jwt.ParseWithClaims(rawAccessToken, claims, func(token *jwt.Token) (interface{}, error) {
		kid, ok := token.Header["kid"]
		if !ok {
			return nil, fmt.Errorf("kid not found")
		}
		jwks, err := s.getJwks()
		if err != nil {
			return nil, fmt.Errorf("getJwks error: %s", err)
		}
		publicKey, err := getPublicKeyFromJwks(jwks, kid.(string))
		if err != nil {
			return nil, fmt.Errorf("getPublicKeyFromJwks error: %s", err)
		}
		return publicKey, nil
	})
	if err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// validate claims that it is an access token
	if claims.Issuer != s.Config.Url {
		http.Error(w, "Invalid issuer", http.StatusUnauthorized)
		return
	}
	if claims.Subject == "" {
		http.Error(w, "Invalid subject", http.StatusUnauthorized)
		return
	}
	if !slices.Contains(claims.Audience, s.Config.Url+"/userinfo") {
		http.Error(w, "Invalid audience", http.StatusUnauthorized)
		return
	}
	usr, err := users.FindUserBySub(claims.Subject)
	if err != nil {
		http.Error(w, "Internal error user not found", http.StatusInternalServerError)
		return
	}

	out, err := json.Marshal(usr)
	if err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(out)
}

func getPublicKeyFromJwks(jwks oidc.Jwks, tokenKid string) (*rsa.PublicKey, error) {
	for _, key := range jwks.Keys {
		if key.Kid == tokenKid {
			keyBytes, err := base64.StdEncoding.DecodeString(key.N)
			if err != nil {
				return nil, fmt.Errorf("base64 decode error: %s", err)
			}
			keyBigInt := big.NewInt(0).SetBytes(keyBytes)
			keyBigInt.SetBytes(keyBytes)
			return &rsa.PublicKey{
				N: keyBigInt,
				E: 65537,
			}, nil
		}
	}
	return nil, fmt.Errorf("key not found in jwks")
}
