package server

import (
	"encoding/base64"
	"encoding/json"
	"github.com/golang-jwt/jwt/v4"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"net/http"
)

func (s *server) getJwks() (oidc.Jwks, error) {
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(s.PrivateKey)
	if err != nil {
		return oidc.Jwks{}, err
	}
	publicKey := privateKey.PublicKey
	jwks := oidc.Jwks{
		Keys: []oidc.JwksKey{
			{
				Kid: "0-0-0-1",
				Kty: "RSA",
				Use: "sig",
				Alg: "RS256",
				N:   base64.StdEncoding.EncodeToString(publicKey.N.Bytes()),
				E:   "AQAB",
			},
		},
	}
	return jwks, nil
}

func (s *server) jwks(w http.ResponseWriter, r *http.Request) {
	jwks, err := s.getJwks()
	if err != nil {
		http.Error(w, "Internal error getting jwks", http.StatusInternalServerError)
		return
	}

	out, err := json.Marshal(jwks)
	if err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(out)
	if err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}

func getJwks() *oidc.Jwks {
	return &oidc.Jwks{
		Keys: []oidc.JwksKey{
			{
				Kid: "0-0-0-1",
				Kty: "RSA",
				Use: "sig",
				Alg: "RS256",
				N:   "n",
				E:   "e",
			},
		},
	}
}
