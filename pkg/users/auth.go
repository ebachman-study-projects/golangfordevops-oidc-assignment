package users

import "fmt"

type User struct {
	Sub               string `json:"sub"`
	Name              string `json:"name"`
	GivenName         string `json:"given_name"`
	FamilyName        string `json:"family_name"`
	PreferredUsername string `json:"preferred_username"`
	Email             string `json:"email"`
	Picture           string `json:"picture"`
}

// Auth (mike): I do not agree with the return types here.
// I would prefer to return an error for error situations (db errors etc),
// and a pointer to a user for success, with a nil for not found.
func Auth(login, password, mfa string) (bool, User, error) {
	if login == "edward" && password == "password" {
		return true, GetAllUsers()[0], nil
	}
	return false, User{}, fmt.Errorf("Invalid login or password")
}

func GetAllUsers() []User {
	return []User{
		{
			Sub:               "9-9-9-9",
			Name:              "edward",
			GivenName:         "Edward",
			FamilyName:        "Viaene",
			PreferredUsername: "edward",
			Email:             "edward@domain.inv",
		},
	}
}

func FindUserBySub(sub string) (User, error) {
	for _, user := range GetAllUsers() {
		if user.Sub == sub {
			return user, nil
		}
	}
	return User{}, fmt.Errorf("User not found")
}
