package main

import (
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/users"
	"html/template"
	"io"
	"net/http"
	"net/url"
)

const redirectUri = "http://localhost:8081/callback"
const discoveryEndpoint = "http://localhost:8888/.well-known/openid-configuration"
const clientId = "1-2-3-4"
const clientSecret = "secret"
const port = "8081"

type app struct {
	Discovery oidc.Discovery
	ClientId  string
	States    map[string]string
}

func newApp(ClientId string) app {
	app := app{
		ClientId: ClientId,
		States:   make(map[string]string),
	}
	return app
}

func (a *app) registerState(state string) {
	a.States[state] = state
}
func (a *app) unregisterState(state string) {
	delete(a.States, state)
}
func (a *app) stateExists(state string) bool {
	_, ok := a.States[state]
	return ok
}

// loadDiscovery loads the discovery document from the OIDC provider
func (a *app) loadDiscovery(endpoint string) error {
	payloadResponse, err := http.Get(endpoint)
	if err != nil {
		return err
	}
	defer payloadResponse.Body.Close()
	if payloadResponse.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code: %d", payloadResponse.StatusCode)
	}
	payload, err := io.ReadAll(payloadResponse.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(payload, &a.Discovery)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	// create a new app and load the discovery document for the OIDC provider
	a := newApp(clientId)
	err := a.loadDiscovery(discoveryEndpoint)
	if err != nil {
		fmt.Printf("loadDiscovery error: %s\n", err)
		return
	}

	http.HandleFunc("/", a.index)
	http.HandleFunc("/callback", a.callback)

	fmt.Println("Listenening at http://localhost:" + port)
	err = http.ListenAndServe(":"+port, nil)
	if err != nil {
		fmt.Printf("ListenAndServe error: %s\n", err)
	}
}

func (a *app) index(w http.ResponseWriter, r *http.Request) {

	authorizeUrl, err := url.ParseRequestURI(a.Discovery.AuthorizationEndpoint)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	state, err := oidc.GetRandomString(64)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	a.registerState(state)

	query := authorizeUrl.Query()
	query.Set("client_id", a.ClientId)
	query.Set("redirect_uri", redirectUri)
	query.Set("response_type", "code")
	query.Set("scope", "openid")
	query.Set("state", state)
	authorizeUrl.RawQuery = query.Encode()
	viewData := struct {
		AuthorizationUrl string
	}{
		AuthorizationUrl: authorizeUrl.String(),
	}
	tplContents := `
<h1>Welcome to the app</h1>
<form method="post" action="{{.AuthorizationUrl}}"><input type="submit" value="Login"></form>
<hr/>
`
	template, err := template.New("index").Parse(tplContents)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/html")
	err = template.Execute(w, viewData)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

}

func (a *app) callback(w http.ResponseWriter, r *http.Request) {
	// Check if the state exists
	if !a.stateExists(r.URL.Query().Get("state")) {
		http.Error(w, "state mismatch error", http.StatusBadRequest)
		return
	}
	a.unregisterState(r.URL.Query().Get("state"))

	tokens, claims, err := getTokenFromCode(
		a.Discovery.TokenEndpoint,
		a.Discovery.JwksURI,
		redirectUri,
		clientId,
		clientSecret,
		r.URL.Query().Get("code"),
	)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if len(tokens) != 2 {
		http.Error(w, "Expected two tokens", http.StatusInternalServerError)
		return
	}
	idToken := tokens[0]
	accessToken := tokens[1]

	w.Write([]byte(
		fmt.Sprintf(
			"ID Token received: %s, ID Token valid: %v, AccessToken valid: %v\n",
			claims.Subject,
			idToken.Valid,
			accessToken.Valid,
		),
	))

	user, err := requestUserInfo(*accessToken)
	if err != nil {
		http.Error(w, "Internal server error - could not request user", http.StatusInternalServerError)
		return
	}

	userJson, err := json.MarshalIndent(user, "", "  ")
	if err != nil {
		http.Error(w, "Internal server error - could not marshal user", http.StatusInternalServerError)
		return
	}
	w.Write([]byte(fmt.Sprintf("Userinfo:\n%s\n", userJson)))

}

// requestUserInfo requests the userinfo from the OIDC provider
// and returns the user information.
func requestUserInfo(accessToken jwt.Token) (users.User, error) {
	user := users.User{}
	req, err := http.NewRequest("GET", "http://localhost:8888/userinfo", nil)
	if err != nil {
		return user, fmt.Errorf("newRequest error: %s", err)
	}
	req.Header.Add("Authorization", "Bearer "+accessToken.Raw)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return user, fmt.Errorf("do request error: %s", err)
	}

	defer res.Body.Close()
	payload, err := io.ReadAll(res.Body)
	if err != nil {
		return user, fmt.Errorf("readAll error: %s", err)
	}
	err = json.Unmarshal(payload, &user)
	if err != nil {
		return user, fmt.Errorf("unmarshal error: %s", err)
	}
	return user, nil
}
