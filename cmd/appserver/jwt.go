package main

import (
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/wardviaene/golang-for-devops-course/oidc-start/pkg/oidc"
	"io"
	"math/big"
	"net/http"
	"net/url"
)

// getTokenFromCode gets token from tokenUrl validating token with jwksUrl and returning token & claims
// the first token returned is the idToken, the second token is the accessToken
// the claims returned are the claims from the idToken.
// In real life, only the access token and its claims would be necessary to execute requests to the OIDC API.
func getTokenFromCode(tokenUrl, jwksUrl, redirectUri, clientID, clientSecret, code string) ([]*jwt.Token, *jwt.RegisteredClaims, error) {
	tokenResponse, err := requestToken(tokenUrl, redirectUri, clientID, clientSecret, code)
	if err != nil {
		return nil, nil, fmt.Errorf("requestToken error: %s", err)
	}

	jwksResponse, err := requestJwks(jwksUrl)
	if err != nil {
		return nil, nil, fmt.Errorf("requestJwks error: %s", err)
	}

	// now validate the token with the jwks keys
	idTokenClaims := &jwt.RegisteredClaims{}
	parsedIdToken, err := jwt.ParseWithClaims(
		tokenResponse.IDToken,
		idTokenClaims,
		func(token *jwt.Token) (interface{}, error) {
			// validate the token with the jwks keys
			tokenKid, ok := token.Header["kid"].(string)
			if !ok {
				return nil, fmt.Errorf("missing key id")
			}
			publicKey, err := getPublicKeyFromJwks(jwksResponse, tokenKid)
			if err != nil {
				return nil, fmt.Errorf("getPublicKeyFromJwks error: %s", err)
			}
			return publicKey, nil
		},
	)
	if err != nil {
		return nil, nil, fmt.Errorf("Id token ParseWithClaims error: %s", err)
	}

	accessTokenClaims := &jwt.RegisteredClaims{}
	parsedAccessToken, err := jwt.ParseWithClaims(
		tokenResponse.AccessToken,
		accessTokenClaims,
		func(token *jwt.Token) (interface{}, error) {
			// validate the token with the jwks keys
			tokenKid, ok := token.Header["kid"].(string)
			if !ok {
				return nil, fmt.Errorf("missing key id")
			}
			publicKey, err := getPublicKeyFromJwks(jwksResponse, tokenKid)
			if err != nil {
				return nil, fmt.Errorf("getPublicKeyFromJwks error: %s", err)
			}
			return publicKey, nil
		},
	)
	if err != nil {
		return nil, nil, fmt.Errorf("Access token ParseWithClaims error: %s", err)
	}

	return []*jwt.Token{parsedIdToken, parsedAccessToken}, idTokenClaims, nil
}

func requestJwks(jwksUrl string) (oidc.Jwks, error) {
	jwks := oidc.Jwks{}

	response, err := http.Get(jwksUrl)
	if err != nil {
		return jwks, err
	}
	defer response.Body.Close()
	bodyString, err := io.ReadAll(response.Body)
	if err != nil {
		return jwks, err
	}
	err = json.Unmarshal(bodyString, &jwks)
	return jwks, err
}

func getPublicKeyFromJwks(jwks oidc.Jwks, tokenKid string) (*rsa.PublicKey, error) {
	for _, key := range jwks.Keys {
		if key.Kid == tokenKid {
			keyBytes, err := base64.StdEncoding.DecodeString(key.N)
			if err != nil {
				return nil, fmt.Errorf("base64 decode error: %s", err)
			}
			keyBigInt := big.NewInt(0).SetBytes(keyBytes)
			keyBigInt.SetBytes(keyBytes)
			return &rsa.PublicKey{
				N: keyBigInt,
				E: 65537,
			}, nil
		}
	}
	return nil, fmt.Errorf("key not found in jwks")
}

func requestToken(tokenUrl, redirectUri, clientID, clientSecret, code string) (oidc.Token, error) {
	t := oidc.Token{}

	form := url.Values{}
	form.Add("grant_type", "authorization_code")
	form.Add("code", code)
	form.Add("redirect_uri", redirectUri)
	form.Add("client_id", clientID)
	form.Add("client_secret", clientSecret)

	response, err := http.Post(
		tokenUrl,
		"application/x-www-form-urlencoded",
		bytes.NewBufferString(form.Encode()),
	)
	if err != nil {
		return t, err
	}
	defer response.Body.Close()
	bodyString, err := io.ReadAll(response.Body)
	if err != nil {
		return t, err
	}
	err = json.Unmarshal(bodyString, &t)
	return t, err
}
