module github.com/wardviaene/golang-for-devops-course/oidc-start

go 1.21

require (
	github.com/go-playground/validator/v10 v10.17.0
	github.com/golang-jwt/jwt/v4 v4.4.2
	github.com/stretchr/testify v1.8.4
	github.com/wardviaene/golang-for-devops-course/ssh-demo v0.0.0-20220616215025-d61a2b0cee5f
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gabriel-vasile/mimetype v1.4.2 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/term v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
)
