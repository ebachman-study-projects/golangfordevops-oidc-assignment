# OIDC Flow Assignment

Implementation of the OIDC assignment of the udemy course
[GoLang for DevOps and Cloud Engineers](https://www.udemy.com/course/golang-for-devops-and-cloud-engineers/learn/lecture/33000512?start=225#questions)
by Edward Viaene.

The assignment is to implement the OIDC flow with a simple authorization server and a simple app server.
The appserver starts with a login button, and on successful login,
the authorization server provides an id and access token to the app server.

Then with the access token, the app server requests user info from the authorization server.

The authorization server provides a discovery endpoint as
a starting point for the app server to find the endpoints for the authorization server.

E.Bachman / 24hoursmedia.

## START SERVERS

Start the authorization server (at port 8888) and the app server (at port 8081) with:

    go run cmd/server/main.go
    go run cmd/appserver/main.go

Browse to localhost:8081, login and use:
* login: edward
* password: password

## Tests

    go test --run TestLoginGet ./pkg/server/
    go test --run TestLoginPost ./pkg/server/
    go test --run TestToken ./pkg/server/
    go test --run TestGetTokenFromCode ./cmd/appserver/
    go test --run TestUserInfo ./cmd/appserver/

# Appserver urls
http://localhost:8081/

## Authorization urls

Example request for authorization:
http://localhost:8888/authorization?client_id=1-2-3-4&redirect_uri=http://localhost:8081/callback&response_type=code&scope=openid&state=1234

Discovery endpoint:
http://localhost:8888/.well-known/openid-configuration

JWKS endpoint:
http://localhost:8888/jwks.json

